"""A Google Cloud Python Pulumi program"""

import pulumi
import pulumi_gcp as gcp
import pulumi_kubernetes as kubernetes
from pulumi_gcp import storage
from pulumi import Output
from pulumi import Config, export, get_project, get_stack, Output, ResourceOptions
from pulumi_gcp.config import project, zone
from pulumi_gcp.container import Cluster, ClusterNodeConfigArgs
from pulumi_kubernetes import Provider
from pulumi_kubernetes.yaml import ConfigGroup
from pulumi_kubernetes.apps.v1 import Deployment, DeploymentSpecArgs
from pulumi_kubernetes.core.v1 import ContainerArgs, PodSpecArgs, PodTemplateSpecArgs, Service, ServicePortArgs, ServiceSpecArgs, Namespace, Secret, ConfigMap
from pulumi_kubernetes.meta.v1 import LabelSelectorArgs, ObjectMetaArgs
from pulumi_kubernetes.rbac.v1 import ClusterRoleBinding, ClusterRole
from pulumi_random import RandomPassword
from pulumi_kubernetes.helm.v3 import Chart, LocalChartOpts, ChartOpts, FetchOpts
from pulumi_kubernetes.helm.v3 import Release, ReleaseArgs, RepositoryOptsArgs
import base64
import pulumi_kubernetes_cert_manager as certmanager
from pulumi_command import local
import time
import hashlib
#from pulumi_kubernetes_cert_manager import CertManager, ReleaseArgs

config = pulumi.Config();

stack = pulumi.get_stack()
sa_name = config.get('serviceaccount_name')
MINIO_BUCKET_NAME = config.get('MINIO_BUCKET_NAME')
proj = config.get('project')
CLUSTER_NAME=f"{proj}-official"
NETWORK_NAME = config.get('NETWORK_NAME')
ISTIO_NAMESPACE = config.get('ISTIO_NAMESPACE')
CERTMANAGER_NAMESPACE = config.get('CERTMANAGER_NAMESPACE')
LETS_ENCRYPT_EMAIL = config.get('LETS_ENCRYPT_EMAIL')
LETS_ENCRYPT_CLOUDDNS_NAME = config.get('LETS_ENCRYPT_CLOUDDNS_NAME')
LETS_ENCRYPT_CLOUDDNS_KEY = config.get('LETS_ENCRYPT_CLOUDDNS_KEY')
ELASTICSEARCH_STORAGE_AMMOUNT = config.get('ELASTICSEARCH_STORAGE_AMMOUNT')
ELASTICSEARCH_VERSION = config.get('ELASTICSEARCH_VERSION')
ELASTICSEARCH_NAMESPACE = config.get('ELASTICSEARCH_NAMESPACE')
ELASTICSEARCH_EXPORTER_PORT = config.get('ELASTICSEARCH_EXPORTER_PORT')
ELASTICSEARCH_EXPORTER_VERSION = config.get('ELASTICSEARCH_EXPORTER_VERSION')
JAEGER_NAMESPACE = config.get('JAEGER_NAMESPACE')
OPENCENSUS_NAMESPACE = config.get('OPENCENSUS_NAMESPACE')
PROMETHEUS_NAMESPACE = config.get('PROMETHEUS_NAMESPACE')
PROMETHEUS_OPERATOR_INSTALL_NAME = config.get('PROMETHEUS_OPERATOR_INSTALL_NAME')
ISTIO_PILOT_TRACE_SAMPLING = config.get('ISTIO_PILOT_TRACE_SAMPLING')
# nodeCount is the number of cluster nodes to provision. Defaults to 3 if unspecified.
NODE_COUNT = config.get_int('NODE_COUNT') or 3
# nodeMachineType is the machine type to use for cluster nodes. Defaults to n1-standard-1 if unspecified.
# See https://cloud.google.com/compute/docs/machine-types for more details on available machine types.
NODE_MACHINE_TYPE = config.get('NODE_MACHINE_TYPE') or 'e2-standard-4'
# username is the admin username for the cluster.
USERNAME = config.get('USERNAME') or 'admin'
# password is the password for the admin user in the cluster.
PASSWORD = config.get_secret('PASSWORD') or RandomPassword("password", length=20, special=True).result
# master version of GKE engine
MASTER_VERSION = config.get('MASTER_VERSION')
# Cluster location
CLUSTER_LOCATION = config.get('CLUSTER_ZONE')

CREATE_NAMESPACE = config.get('CREATE_NAMESPACE')
INIT_NAMESPACE = config.get('INIT_NAMESPACE')
CREATE_CLUSTER = config.get('CREATE_CLUSTER')
SERVICES_NAMESPACE = config.get('SERVICES_NAMESPACE')
DNS_BASE_NAME = config.get('DNS_BASE_NAME')
DOMAIN_NAME = f'{SERVICES_NAMESPACE}.{DNS_BASE_NAME}'
DNS_ZONE_DESCRIPTION = config.get('DNS_ZONE_DESCRIPTION')
DNS_ZONE_VISIBILITY = config.get('DNS_ZONE_VISIBILITY')
DNS_ZONE_NAME = config.get('DNS_ZONE_NAME')

DOCKER_SERVER=config.get('DOCKER_SERVER')
DOCKER_USERNAME=config.get('DOCKER_USERNAME')
DOCKER_PASSWORD=config.get('DOCKER_PASSWORD')
DOCKER_EMAIL=config.get('DOCKER_EMAIL')
DOCKER_TOKEN=config.get('DOCKER_TOKEN')

AUTH_JWT_SECRET=config.get('AUTH_JWT_SECRET')
AUTH_SECRET_NAME=config.get('AUTH_SECRET_NAME')

MINIO_ACCESS_KEY=config.get('MINIO_ACCESS_KEY')
MINIO_GCS_SECRET_NAME=config.get('MINIO_GCS_SECRET_NAME')
SORS_USERS_SECRET_NAME=config.get('SORS_USERS_SECRET_NAME')
RABBITMQ_PASSWORD=config.get_secret('RABBITMQ_PASSWORD')

SORS_USERS=config.get('SORS_USERS')

PG_VERSION=config.get('PG_VERSION')
PG_INSTANCE_NAME=config.get('PG_INSTANCE_NAME')

# Create a GCP Service Account
dns_service_account = gcp.serviceaccount.Account("dns_service_account",
    account_id=f'{sa_name}-dns-{stack}',
    display_name="A service account for a GKE application")

#letsencrypt_dns_sa_key = gcp.serviceaccount.Key("letsencrypt_dns_sa_key",
#    service_account_id=dns_service_account.name,
#    public_key_type="TYPE_X509_PEM_FILE",
#    opts=ResourceOptions(additional_secret_outputs=['private_key']))

letsencrypt_dns_sa_key = gcp.serviceaccount.Key("letsencrypt_dns_sa_key",
    service_account_id=dns_service_account.name,
    public_key_type="TYPE_X509_PEM_FILE",
    )

member=[Output.concat("serviceAccount:", dns_service_account.email)]
#pulumi.export('member', member)
project = gcp.projects.IAMBinding("project",
    project=proj,
    role="roles/dns.admin",
    members=member)

# Create a Network
vpc_network = gcp.compute.Network(NETWORK_NAME,
        auto_create_subnetworks='true',
        name=NETWORK_NAME,
        routing_mode='REGIONAL',
        description=f"{proj} Network",
        project=proj
        )

# Name Firewall Rules
firewall_rule_1=Output.concat(NETWORK_NAME,'-allow-icmp-2')
firewall_rule_2=Output.concat(NETWORK_NAME,'-allow-internal-2')
firewall_rule_3=Output.concat(NETWORK_NAME,'-allow-rdp-2')
firewall_rule_4=Output.concat(NETWORK_NAME,'-allow-ssh-2')

# Create Firewall Rules
firewall_rule_1 = gcp.compute.Firewall("firewall_rule_1",
    network=vpc_network.name,
    name=firewall_rule_1,
    direction='INGRESS',
    priority=65534,
    project=proj,
    source_ranges=['0.0.0.0/0'],
    allows=[
        gcp.compute.FirewallAllowArgs(
            protocol="icmp",
        ),
    ],
    )

firewall_rule_2 = gcp.compute.Firewall("firewall_rule_2",
    network=vpc_network.name,
    name=firewall_rule_2,
    direction='INGRESS',
    priority=65534,
    project=proj,
    source_ranges=['10.128.0.0/9'],
    allows=[
        gcp.compute.FirewallAllowArgs(
            protocol="all",
        ),
    ],
    )

firewall_rule_3 = gcp.compute.Firewall("firewall_rule_3",
    network=vpc_network.name,
    name=firewall_rule_3,
    direction='INGRESS',
    priority=65534,
    project=proj,
    source_ranges=['0.0.0.0/0'],
    allows=[
        gcp.compute.FirewallAllowArgs(
            protocol="tcp",
            ports=[
                "3389",
            ],
        ),
    ],
    )

firewall_rule_4 = gcp.compute.Firewall("firewall_rule_4",
    network=vpc_network.name,
    name=firewall_rule_4,
    direction='INGRESS',
    priority=65534,
    project=proj,
    source_ranges=['0.0.0.0/0'],
    allows=[
        gcp.compute.FirewallAllowArgs(
            protocol="tcp",
             ports=[
                "22",
            ],
        ),
    ],
    )



# Now, actually create the GKE cluster.
k8s_cluster = Cluster('gke-cluster',
    name=f"{proj}-official",
    initial_node_count=NODE_COUNT,
    project=proj,
    logging_service='logging.googleapis.com/kubernetes',
    location=CLUSTER_LOCATION,
    network=vpc_network.name,
    ip_allocation_policy=gcp.container.ClusterIpAllocationPolicyArgs(cluster_ipv4_cidr_block="10.8.0.0/14", services_ipv4_cidr_block="10.12.0.0/20"),
    networking_mode='VPC_NATIVE',
    node_version=MASTER_VERSION,
    min_master_version=MASTER_VERSION,
    node_config=ClusterNodeConfigArgs(
        machine_type=NODE_MACHINE_TYPE,
        oauth_scopes=[
            'https://www.googleapis.com/auth/compute',
            'https://www.googleapis.com/auth/devstorage.read_only',
            'https://www.googleapis.com/auth/logging.write',
            'https://www.googleapis.com/auth/monitoring'
        ],
    ),
)

k8s_info = Output.all(k8s_cluster.name, k8s_cluster.endpoint, k8s_cluster.master_auth)
k8s_config = k8s_info.apply(
    lambda info: """apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: {0}
    server: https://{1}
  name: {2}
contexts:
- context:
    cluster: {2}
    user: {2}
  name: {2}
current-context: {2}
kind: Config
preferences: {{}}
users:
- name: {2}
  user:
    auth-provider:
      config:
        cmd-args: config config-helper --format=json
        cmd-path: gcloud
        expiry-key: '{{.credential.token_expiry}}'
        token-key: '{{.credential.access_token}}'
      name: gcp
""".format(info[2]['cluster_ca_certificate'], info[1], '{0}_{1}_{2}'.format(project, zone, info[0])))

# Make a Kubernetes provider instance that uses our cluster from above.
k8s_provider = Provider('gke_k8s', kubeconfig=k8s_config)

# Export Cluster Name
pulumi.export('cluster_name', k8s_cluster.name)

OTDR_SA_SECRET="sqlproxy-secret"
OTDR_SA_NAME=f"otdr-{CLUSTER_NAME}"
OTDR_SA_DESC="Used by OTDR-MS to access PostgreSQL"
OTDR_PG_IAM_USER=f"{OTDR_SA_NAME}@{proj}.iam"
#OTDR_SA_KEY_FILE="service_account_credentials.json"
# OTDR_SA_ROLES=("roles/cloudsql.client" "roles/cloudsql.instanceUser")

# OTDR_PG_DATABASE="otdrservice_$NAMESPACE"
# OTDR_PG_INSTANCE="$GKE_PROJECT-sql"
# OTDR_PG_DB_VERSION="POSTGRES_9_6"

otdr_account = gcp.serviceaccount.Account("otdr-account",
    account_id=OTDR_SA_NAME,
    description=OTDR_SA_DESC,
    disabled=False,
    display_name=OTDR_SA_NAME,
    project=proj,
    )

otdr_member=[Output.concat("serviceAccount:", otdr_account.email)]

otdr_account_iam_sql = gcp.projects.IAMBinding("otdr-account-iam-sql",
    project=proj,
    role="roles/cloudsql.client",
    members=otdr_member)

otdr_account_iam_sql_user = gcp.projects.IAMBinding("otdr-account-iam-sql-user",
    project=proj,
    role="roles/cloudsql.instanceUser",
    members=otdr_member)

### Create Service accounts and Roles
REPORT_SA_NAME=f"report-{CLUSTER_NAME}"
#REPORT_SA_EMAIL=f"{REPORT_SA_NAME}@{proj}.iam.gserviceaccount.com"
REPORT_SA_DESC="Report Service - Service Account"
# REPORT_SA_ROLES=["roles/cloudsql.client", "roles/cloudkms.cryptoKeyDecrypter", "roles/storage.objectCreator", "roles/storage.objectViewer"]
REPORT_SA_SECRET="report-serviceaccount-credentials"

report_account = gcp.serviceaccount.Account("report-account",
    account_id=REPORT_SA_NAME,
    description=REPORT_SA_DESC,
    disabled=False,
    display_name=REPORT_SA_NAME,
    project=proj,
    )

report_member=[Output.concat("serviceAccount:", report_account.email)]

report_account_iam_sql = gcp.projects.IAMBinding("report-account-iam-sql",
    project=proj,
    role="roles/cloudsql.client",
    members=report_member)

report_account_iam_decrypter = gcp.projects.IAMBinding("report-account-iam-decrypter",
    project=proj,
    role="roles/cloudkms.cryptoKeyDecrypter",
    members=report_member)

report_account_iam_obj_creator = gcp.projects.IAMBinding("report-account-iam-obj-creator",
    project=proj,
    role="roles/storage.objectCreator",
    members=report_member)

report_account_iam_obj_view = gcp.projects.IAMBinding("report-account-iam-obj-view",
    project=proj,
    role="roles/storage.objectViewer",
    members=report_member)

PROJECT_SA_NAME=f"project-{CLUSTER_NAME}"
#REPORT_SA_EMAIL=f"{REPORT_SA_NAME}@{proj}.iam.gserviceaccount.com"
PROJECT_SA_DESC="Project Service - Service Account"
#PROJECT_SA_ROLES=["roles/cloudsql.client", "roles/cloudkms.cryptoKeyDecrypter", "roles/storage.objectCreator", "roles/storage.objectViewer"]
PROJECT_SA_SECRET="project-serviceaccount-credentials"

project_account = gcp.serviceaccount.Account("project-account",
    account_id=PROJECT_SA_NAME,
    description=PROJECT_SA_DESC,
    disabled=False,
    display_name=PROJECT_SA_NAME,
    project=proj,
    )

project_member=[Output.concat("serviceAccount:", project_account.email)]

project_account_iam_sql = gcp.projects.IAMBinding("project-account-iam-sql",
    project=proj,
    role="roles/cloudsql.client",
    members=report_member)

project_account_iam_decrypter = gcp.projects.IAMBinding("project-account-iam-decrypter",
    project=proj,
    role="roles/cloudkms.cryptoKeyDecrypter",
    members=report_member)

project_account_iam_obj_creator = gcp.projects.IAMBinding("project-account-iam-obj-creator",
    project=proj,
    role="roles/storage.objectCreator",
    members=report_member)

project_account_iam_obj_view = gcp.projects.IAMBinding("project-account-iam-obj-view",
    project=proj,
    role="roles/storage.objectViewer",
    members=report_member)


### Database Creation
postgres_instance = gcp.sql.DatabaseInstance("afl-box-sql-14",
    database_version="POSTGRES_14",
    master_instance_name="",
    name=PG_INSTANCE_NAME,
    project=proj,
    region="us-central1",
    settings=gcp.sql.DatabaseInstanceSettingsArgs(
        activation_policy="ALWAYS",
        availability_type="ZONAL",
        backup_configuration=gcp.sql.DatabaseInstanceSettingsBackupConfigurationArgs(
            backup_retention_settings=gcp.sql.DatabaseInstanceSettingsBackupConfigurationBackupRetentionSettingsArgs(
                retained_backups=7,
                retention_unit="COUNT",
            ),
            binary_log_enabled=False,
            enabled=True,
            location="us",
            point_in_time_recovery_enabled=True,
            start_time="11:00",
            transaction_log_retention_days=7,
        ),
        collation="",
        database_flags=[gcp.sql.DatabaseInstanceSettingsDatabaseFlagArgs(
            name="cloudsql.iam_authentication",
            value="on",
        )],
        disk_autoresize=True,
        disk_autoresize_limit=0,
        disk_size=100,
        disk_type="PD_SSD",
        insights_config=gcp.sql.DatabaseInstanceSettingsInsightsConfigArgs(
            query_insights_enabled=True,
            query_string_length=1024,
            record_application_tags=True,
            record_client_address=False,
        ),
        ip_configuration=gcp.sql.DatabaseInstanceSettingsIpConfigurationArgs(
            authorized_networks=[],
            ipv4_enabled=True,
            private_network="",
            require_ssl=False,
        ),
        location_preference=gcp.sql.DatabaseInstanceSettingsLocationPreferenceArgs(
            follow_gae_application="",
            zone="us-central1-b",
        ),
        maintenance_window=gcp.sql.DatabaseInstanceSettingsMaintenanceWindowArgs(
            day=7,
            hour=3,
            update_track="",
        ),
        pricing_plan="PER_USE",
        tier="db-custom-1-3840",
        user_labels={},
    ),
    )

### Create Database Users

pg_report_user = gcp.sql.User("pg-report-user",
    instance=postgres_instance.name,
    name='reportagent',
    password=config.get_secret("reportagent-password"))

pg_project_user = gcp.sql.User("pg-project-user",
    instance=postgres_instance.name,
    name='projectagent',
    password=config.get_secret("projectagent-password"))

pg_otdr_user = gcp.sql.User("pg-otdr-user",
    instance=postgres_instance.name,
    name=OTDR_PG_IAM_USER,
    type="CLOUD_IAM_SERVICE_ACCOUNT",
    )

### Create DNS Zone ###
dns_zone = gcp.dns.ManagedZone("Dns-Zone",
    description=f"{DNS_ZONE_DESCRIPTION}",
    dns_name=f"{DNS_BASE_NAME}.",
    dnssec_config=gcp.dns.ManagedZoneDnssecConfigArgs(
        default_key_specs=[
            gcp.dns.ManagedZoneDnssecConfigDefaultKeySpecArgs(
                algorithm="rsasha256",
                key_length=2048,
                key_type="keySigning",
                kind="dns#dnsKeySpec",
            ),
            gcp.dns.ManagedZoneDnssecConfigDefaultKeySpecArgs(
                algorithm="rsasha256",
                key_length=1024,
                key_type="zoneSigning",
                kind="dns#dnsKeySpec",
            ),
        ],
        kind="dns#managedZoneDnsSecConfig",
        non_existence="nsec3",
        state="on",
    ),
    force_destroy=False,
    labels={},
    name=f"{DNS_ZONE_NAME}",
    project="afl-box",
    reverse_lookup=False,
    visibility=f"{DNS_ZONE_VISIBILITY}",
)

#Create Kubernetes Namespace
istio_ns = Namespace('istio-ns',
        metadata={
            "name" : 'istio-system',
    }, #opts=ResourceOptions(provider=k8s_provider)
)
istio_clusterrole = ClusterRoleBinding('istio_clusterrole',
        metadata={
            "name" : 'cluster-admin-binding'
        },
        role_ref={
            "apiGroup" : "rbac.authorization.k8s.io",
            "kind" : "ClusterRole",
            "name" : "cluster-admin",
        },
        subjects=[
            {
                "kind": "User",
                "name": USERNAME,

            }
        ], #opts=ResourceOptions(provider=k8s_provider)
    )

#
# istio_base = Chart(
#     "istio-base",
#     LocalChartOpts(
#         path="./istio-1.12.0/manifests/charts/base",
#         namespace="istio-system",
#     ), #opts=ResourceOptions(provider=k8s_provider)
# )

istio_base = Release(
    "istio-base",
    ReleaseArgs(
        chart="./istio-1.12.0/manifests/charts/base",
        namespace="istio-system",
    ), ##opts=ResourceOptions(provider=k8s_provider)
)

# istio_base = Release(
#     "istio-base",
#     ReleaseArgs(
#         chart="base",
#         repository_opts=RepositoryOptsArgs(
#             repo="https://istio-release.storage.googleapis.com/charts",
#         ),
#         namespace="istio-system",
#     ), #opts=ResourceOptions(provider=k8s_provider)
# )
#

istio_discovery = Release(
    "istio-discovery",
    ReleaseArgs(
        chart="./istio-1.12.0/manifests/charts/istio-control/istio-discovery",
        namespace="istio-system",
        values={
            'global':{
                'tracer':{
                    'zipkin':{
                        'address':'jaeger-collector.observability.svc.cluster.local:9411',
                    },
                },
            },
            'pilot':{
                'traceSampling': ISTIO_PILOT_TRACE_SAMPLING,
            },
        },
    ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[istio_base])
)
#
#
# istio_discovery = Chart(
#     "istio-discovery",
#     LocalChartOpts(
#         path="./istio-1.12.0/manifests/charts/istio-control/istio-discovery",
#         namespace="istio-system",
#         values={
#             'global':{
#                 'tracer':{
#                     'zipkin':{
#                         'address':'jaeger-collector.observability.svc.cluster.local:9411',
#                         },
#                     },
#                 },
#             'pilot':{
#                 'traceSampling': ISTIO_PILOT_TRACE_SAMPLING,
#                 },
#             },
#     ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[istio_base])
# )
#
istio_ingress = Release(
    "istio-ingress",
    ReleaseArgs(
        chart="./istio-1.12.0/manifests/charts/gateways/istio-ingress",
        namespace="istio-system",
    ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[istio_base,istio_discovery])
)

srv = Service.get("istio-ingressgateway", "istio-system/istio-ingressgateway")
# Export the ingress IP for Wordpress frontend.
pulumi.export("frontendIP", srv.status.load_balancer.ingress[0].ip)
# istio_ingress = Chart(
#     "istio-ingress",
#     LocalChartOpts(
#         path="./istio-1.12.0/manifests/charts/gateways/istio-ingress",
#         namespace="istio-system"
#     ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[istio_base,istio_discovery])
# )

kiali_operator_ns = Namespace('kiali-operator-ns',
        metadata={
            "name" : 'kiali-operator',
    }
)
#
# kiali_operator = Release(
#     "kiali-operator",
#     ReleaseArgs(
#         chart="kiali-operator",
#         namespace=kiali_operator_ns.metadata.name,
#         name="kiali-operator",
#         values={
#             'cr':{
#                 'create':'true',
#             },
#             'cr':{
#                 'namespace':'istio-system',
#             },
#         },
#         repository_opts=RepositoryOptsArgs(
#             repo="https://kiali.org/helm-charts",
#         ),
#     ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[istio_ns])
# )

if CERTMANAGER_NAMESPACE != ISTIO_NAMESPACE:
    certmanager_ns = Namespace('certmanager_ns',
        metadata={
            "name" : CERTMANAGER_NAMESPACE,
    })

certmanager = Release(
    "certmanager",
    ReleaseArgs(
        chart="cert-manager",
        version="v1.4.0",
        name="certmanager",
        namespace=CERTMANAGER_NAMESPACE,
        values={
            'installCRDs':'true',
        },
        repository_opts=RepositoryOptsArgs(
            repo="https://charts.jetstack.io",
        ),
    ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[istio_base, istio_discovery])
)
# certmanager = Chart(
#     "certmanager",
#     ChartOpts(
#         chart="cert-manager",
#         namespace=certmanager_namespace,
#         values={
#             'installCRDs':'true',
#             },
#         fetch_opts=FetchOpts(
#             repo="https://charts.jetstack.io",
#         ),
#     ), #opts=ResourceOptions(provider=k8s_provider, depends_on=[istio_base,istio_discovery])
# )
#
#
GCE_PROJECT_BYTES=base64.b64encode(proj.encode("utf-8"))
GCE_PROJECT=str(GCE_PROJECT_BYTES, "utf-8")
#
letsencrypt_secret = Secret("letsencrypt_secret",
        type='generic',
        data={
            "GCE_PROJECT": GCE_PROJECT,
            "clouddns-key.json": letsencrypt_dns_sa_key.private_key,
        },
        metadata={
            "name" : 'clouddns',
            "namespace": 'istio-system',
        },
)
#
# #ClusterIssuer_info = Output.all(letsencrypt_dns_sa_key.name, proj, letsencrypt_secret.metadata.name, lets_enc_email)
# #ClusterIssuer_config = ClusterIssuer_info.apply(
# #    lambda info: """apiVersion: cert-manager.io/v1
# #kind: ClusterIssuer
# #metadata:
# #  name: letsencrypt-dns
# #  namespace: istio-system
# #spec:
# #  acme:
# #    server: https://acme-v02.api.letsencrypt.org/directory
# #    email: {3}
# #    privateKeySecretRef:
# #      name: letsencrypt
# #    solvers:
# #    - dns01:
# #        clouddns:
# #          project: {1}
# #          serviceAccountSecretRef:
# #            name: {2}
# #            key: {0}
# #""".format(info[0], info[1], info[2], info[3]))
#
# #render_provider = Provider('k8s-yaml-rendered',
# #    render_yaml_to_directory='yaml')
#
create_clusterissuer_template = local.Command("create-clusterissuer-template",
    create=f"renderizer -C --letsEncrypt_email={LETS_ENCRYPT_EMAIL} --gcpProject={proj} --letsEncrypt_provider_clouddns_name={LETS_ENCRYPT_CLOUDDNS_NAME} --letsEncrypt_provider_clouddns_key={LETS_ENCRYPT_CLOUDDNS_KEY} ./yaml/cluster-issuer-template.yaml > ./gen-files/cluster-issuer.yaml"
)
#
# Create ClusterIssuer from File
dns_cluster_issuer = kubernetes.yaml.ConfigFile('dns_cluster_issuer', './gen-files/cluster-issuer.yaml')

appLabels = { "istio-injection": "enabled" }
elastic_ns = Namespace('elastic-ns',
        metadata={
            "name" : ELASTICSEARCH_NAMESPACE,
            "labels" : appLabels,
    })
#
#
elasticsearch = Release(
    "elasticsearch",
    ReleaseArgs(
        chart="elasticsearch",
        namespace=ELASTICSEARCH_NAMESPACE,
        name="elasticsearch",
        values={
            'volumeClaimTemplate': {
                'resources':{
                    'requests':{
                        'storage': ELASTICSEARCH_STORAGE_AMMOUNT,
                    }

                },
            },
            'imageTag' : ELASTICSEARCH_VERSION,
            'roles':{
                    'ml' : 'false',
            },
        },
        repository_opts=RepositoryOptsArgs(
            repo="https://helm.elastic.co",
        ),
    ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[elastic_ns])
)
# elasticsearch = Chart(
#     "elasticsearch",
#     ChartOpts(
#         chart="elasticsearch",
#         namespace=ELASTICSEARCH_NAMESPACE,
#         values={
#             'volumeClaimTemplate': {
#                 'resources':{
#                     'storage': ELASTICSEARCH_STORAGE_AMMOUNT,
#                 },
#             },
#             'imageTag' : ELASTICSEARCH_VERSION,
#             'roles':{
#                     'ml' : 'false',
#             },
#         },
#         fetch_opts=FetchOpts(
#             repo="https://helm.elastic.co",
#         ),
#     ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[elastic_ns])
# )
#
#
elasticsearch_exporter = Release(
    "elasticsearch-exporter",
    ReleaseArgs(
        chart="prometheus-elasticsearch-exporter",
        namespace=ELASTICSEARCH_NAMESPACE,
        name="elasticsearch-exporter",
        values={
            'service' :{
                'httpPort': ELASTICSEARCH_EXPORTER_PORT,
            },
            'es':{
                'uri': f'http://elasticsearch-master.ELASTICSEARCH_NAMESPACE.svc:9200',
                },
        },
        repository_opts=RepositoryOptsArgs(
            repo="https://prometheus-community.github.io/helm-charts",
        ),
    ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[elastic_ns,elasticsearch])
)
#
# elasticsearch_exporter = Chart(
#     "elasticsearch-exporter",
#     ChartOpts(
#         chart="prometheus-elasticsearch-exporter",
#         namespace=ELASTICSEARCH_NAMESPACE,
#         values={
#             #'image':{
#             #    'tag': ELASTICSEARCH_EXPORTER_VERSION,
#             #},
#             'podAnnotations':{
#                 'prometheus.io/port': ELASTICSEARCH_EXPORTER_PORT,
#             },
#             'service' :{
#                 'httpPort': ELASTICSEARCH_EXPORTER_PORT,
#             },
#             'es':{
#                 'uri': f'http://elasticsearch-master.ELASTICSEARCH_NAMESPACE.svc:9200',
#                 },
#             },
#         fetch_opts=FetchOpts(
#             repo="https://prometheus-community.github.io/helm-charts",
#         ),
#     ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[elastic_ns,elasticsearch])
# )
#
#
kibana = Release(
    "kibana",
    ReleaseArgs(
        chart="kibana",
        namespace=ELASTICSEARCH_NAMESPACE,
        name="kibana",
        values={
            'imageTag' : ELASTICSEARCH_VERSION,
            'elasticsearchHosts' : 'http://elasticsearch-master.elastic.svc:9200',
            },
        repository_opts=RepositoryOptsArgs(
            repo="https://helm.elastic.co",
        ),
    ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[elastic_ns,elasticsearch])
)
#
# kibana = Chart(
#     "kibana",
#     ChartOpts(
#         chart="kibana",
#         namespace=ELASTICSEARCH_NAMESPACE,
#         values={
#             'imageTag' : '7.12.0',
#             'elasticsearchHosts' : 'http://elasticsearch-master.elastic.svc:9200',
#             },
#         fetch_opts=FetchOpts(
#             repo="https://helm.elastic.co",
#         ),
#     ), #opts=ResourceOptions(provider=k8s_provider)
# )
#
jaeger_ns = Namespace('jaeger-ns',
        metadata={
            "name" : JAEGER_NAMESPACE,
            "labels" : appLabels,
    })
#
# ### This part is on construction, the jaeger operator wont deploy with pulumi - I deployed it manually
# #helm install --wait jaeger jaegertracing/jaeger-operator --namespace observability
# #jaeger_operator = Chart(
# #    "jaeger-operator",
# #    ChartOpts(
# #        chart="jaeger-operator",
# #        namespace=JAEGER_NAMESPACE,
# #        fetch_opts=FetchOpts(
# #            repo="https://jaegertracing.github.io/helm-charts",
# #        ),
# #    ), #opts=ResourceOptions(provider=k8s_provider)
# #)
#
jaeger_operator = Release(
    "jaeger-operator",
    ReleaseArgs(
        chart="jaeger-operator",
        namespace=JAEGER_NAMESPACE,
        name="jaeger-operator",
        version="2.28.0",
        repository_opts=RepositoryOptsArgs(
            repo="https://jaegertracing.github.io/helm-charts",
        ),
    ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[jaeger_ns])
)
# #jaeger = Chart(
# #    "jaeger",
# #    ChartOpts(
# #        chart="jaeger",
# #        namespace=JAEGER_NAMESPACE,
# #        values={
# #            'provisionDataStore': {
# #                'cassandra': False,
# #                },
# #            'storage':{
# #                'type': 'elasticsearch',
# #                'elasticsearch': {
# #                    'host' : 'elasticsearch-master.elastic.svc',
# #                    'port' : '9200',
# #                    },
# #                },
# #            'esIndexCleaner': {
# #                'enabled' : 'false',
# #                'schedule': '55 23 * * *',
# #                'numberOfDays': '90',
# #                },
# #            },
# #        fetch_opts=FetchOpts(
# #            repo="https://jaegertracing.github.io/helm-charts",
# #        ),
# #    ), #opts=ResourceOptions(provider=k8s_provider)
# #)
#
jaeger = kubernetes.yaml.ConfigFile('jaeger', './yaml/jaeger.yaml', #opts=ResourceOptions(provider=k8s_provider,depends_on=[jaeger_ns,jaeger_operator]))
#
if OPENCENSUS_NAMESPACE != JAEGER_NAMESPACE:
    opencensus_ns = Namespace('opencensus-ns',
        metadata={
            "name" : OPENCENSUS_NAMESPACE,
            "labels" : appLabels,
    })

opencensus_agent = kubernetes.yaml.ConfigGroup('opencensus-agent',
        files=['./yaml/opencensus/opencensus-agent-configmap.yaml','./yaml/opencensus/oc-agent/daemonset.yaml','./yaml/opencensus/oc-agent/service.yaml'],)

opencensus_collector = kubernetes.yaml.ConfigGroup('opencensus-collector',
        files=['./yaml/opencensus/opencensus-collector-configmap.yaml','./yaml/opencensus/oc-collector/deployment.yaml','./yaml/opencensus/oc-collector/service.yaml'],)


prometheus_ns = Namespace('prometheus-ns',
        metadata={
            "name" : PROMETHEUS_NAMESPACE,
            "labels" : appLabels,
    })
#
#prometheus_msteams = Chart(
#    "prometheus-msteams",
#    LocalChartOpts(
#        path="./yaml/prometheus-msteams",
#        namespace= PROMETHEUS_NAMESPACE,
#        values={
#            'connectors':{
#                '- alertmanager': 'https://outlook.office.com/webhook/b7e42374-4c09-4e2b-83d2-58f4b8112b2c@b170a9c1-0e3f-494f-9d4e-bd58fa9bc781/IncomingWebhook/b86416a0a2bd4688b930b80dcf7b582e/cf62629a-1f1a-49e2-a542-7519f05f76a2'
#                },
#            },
#    ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[prometheus_ns])
#)
#
# prometheus_msteams = Chart(
#     "prometheus-msteams",
#     ChartOpts(
#         chart="prometheus-msteams",
#         namespace=PROMETHEUS_NAMESPACE,
#         values={
#             'connectors': [ {'alertmanager': 'https://outlook.office.com/webhook/b7e42374-4c09-4e2b-83d2-58f4b8112b2c@b170a9c1-0e3f-494f-9d4e-bd58fa9bc781/IncomingWebhook/b86416a0a2bd4688b930b80dcf7b582e/cf62629a-1f1a-49e2-a542-7519f05f76a2'}]
#         },
#         fetch_opts=FetchOpts(
#             repo="https://prometheus-msteams.github.io/prometheus-msteams/",
#         ),
#     ), #opts=ResourceOptions(provider=k8s_provider)
# )
prometheus_msteams = Release(
    "prometheus-msteams",
    ReleaseArgs(
        chart="./prometheus-stack/prometheus-msteams",
        name="prometheus-msteams",
        values={
            'connectors': [ {'alertmanager': 'https://outlook.office.com/webhook/b7e42374-4c09-4e2b-83d2-58f4b8112b2c@b170a9c1-0e3f-494f-9d4e-bd58fa9bc781/IncomingWebhook/b86416a0a2bd4688b930b80dcf7b582e/cf62629a-1f1a-49e2-a542-7519f05f76a2'}]
        },
        namespace="monitoring",
    ), #opts=ResourceOptions(provider=k8s_provider,depends_on=[prometheus_ns])
)

# #
alertmanager_encoded = 'Z2xvYmFsOgogIHJlc29sdmVfdGltZW91dDogNW0KcmVjZWl2ZXJzOgotIG5hbWU6IHByb21ldGhldXMtbXN0ZWFtcwogIHdlYmhvb2tfY29uZmlnczoKICAtIHVybDogImh0dHA6Ly9wcm9tZXRoZXVzLW1zdGVhbXM6MjAwMC9hbGVydG1hbmFnZXIiCiAgICBzZW5kX3Jlc29sdmVkOiB0cnVlICAgIAotIG5hbWU6ICJudWxsIgpyb3V0ZToKICBncm91cF9ieToKICAtIGNsdXN0ZXIKICAtIGFsZXJ0bmFtZQogIGdyb3VwX2ludGVydmFsOiA1bQogIGdyb3VwX3dhaXQ6IDMwcwogIHJlY2VpdmVyOiBwcm9tZXRoZXVzLW1zdGVhbXMKICByZXBlYXRfaW50ZXJ2YWw6IDRoCiAgcm91dGVzOgogICAgIyBVbnRpbCB3ZSBpbXBsZW1lbnQgc3VwcG9ydCBmb3IgYSAnZGVhZCBtYW4gc3dpdGNoJyBvZmYgb2YgdGhpcyBhbGVydCB3ZSBhcmUgZ29pbmcgdG8gcHJldmVudCBpdCBmcm9tIGNvbnN0YW50bHkgc2hvd2luZyB1cCBpbiBvdXIgYWxlcnRzIGZlZWQuCiAgLSBtYXRjaDoKICAgICAgYWxlcnRuYW1lOiBXYXRjaGRvZwogICAgcmVjZWl2ZXI6ICJudWxsIgogIC0gbWF0Y2g6CiAgICAgIGFsZXJ0bmFtZTogQ1BVVGhyb3R0bGluZ0hpZ2ggCiAgICAgIG5hbWVzcGFjZToga3ViZS1zeXN0ZW0KICAgIHJlY2VpdmVyOiAibnVsbCIgIA=='

alertmanager_secret = Secret("alertmanager-secret",
        type='generic',
        data={
            "alertmanager.yaml": alertmanager_encoded,
        },
        metadata={
            "name" : 'alertmanager-prometheus-stack-custom-prometheus-op-alertmanager',
            "namespace": PROMETHEUS_NAMESPACE,
        },
)
#
#
#

prometheus_stack = Release(
   "prometheus-stack",
   ReleaseArgs(
       chart="kube-prometheus-stack",
       namespace=PROMETHEUS_NAMESPACE,
       name="prometheus-stack",
       #value_yaml_files=pulumi.FileAsset("./kube-prometheus-stack-values.yml
       values={
          "prometheus": {
            "prometheusSpec": {
              "storageSpec": {
                "volumeClaimTemplate": {
                  "spec": {
                    "storageClassName": "standard",
                    "accessModes": [
                      "ReadWriteOnce"
                    ],
                    "resources": {
                      "requests": {
                        "storage": "200Gi",
                      },
                    },
                  },
                },
              },
              "retention": "26w",
              "scrapeInterval": "15s",
            },
          },
          "alertmanager": {
            "alertmanagerSpec": {
              "configSecret": "alertmanager-prometheus-stack-custom-prometheus-op-alertmanager"
            },
          },
          "grafana": {
            "persistence": {
              "type": "statefulset",
              "enabled": 'true',
            },
            "dashboardProviders": {
              "dashboardproviders.yaml": {
                "apiVersion": 1,
                "providers": [
                  {
                    "allowUiUpdates": 'true',
                    "disableDeletion": 'true',
                    "editable": 'true',
                    "folder": "Istio",
                    "folderUid": "",
                    "name": "istio",
                    "options": {
                      "path": "/var/lib/grafana/dashboards/istio"
                    },
                    "orgId": 1,
                    "type": "file",
                    "updateIntervalSeconds": 10,
                  },
                  {
                    "allowUiUpdates": 'true',
                    "disableDeletion": 'true',
                    "editable": 'true',
                    "folder": "Community",
                    "folderUid": "",
                    "name": "community",
                    "options": {
                      "path": "/var/lib/grafana/dashboards/community"
                    },
                    "orgId": 1,
                    "type": "file",
                    "updateIntervalSeconds": 10,
                  },
                  {
                    "allowUiUpdates": 'true',
                    "disableDeletion": 'true',
                    "editable": 'true',
                    "folder": "Custom",
                    "folderUid": "",
                    "name": "custom",
                    "options": {
                      "path": "/tmp/dashboards/custom"
                    },
                    "orgId": 1,
                    "type": "file",
                    "updateIntervalSeconds": 10,
                  },
                ]
              },
            },
            "dashboards": {
              "community": {
                "elastisearch": {
                  "datasource": "Prometheus",
                  "gnetId": 2322,
                  "revision": 4,
                },
              },
              "istio": {
                "istio_galley": {
                  "datasource": "Prometheus",
                  "gnetId": 7648,
                  "revision": 1,
                },
                "istio_mesh": {
                  "datasource": "Prometheus",
                  "gnetId": 7639,
                  "revision": 1,
                },
                "istio_mixer": {
                  "datasource": "Prometheus",
                  "gnetId": 7642,
                  "revision": 1,
                },
                "istio_pilot": {
                  "datasource": "Prometheus",
                  "gnetId": 7645,
                  "revision": 1,
                },
                "istio_service": {
                  "datasource": "Prometheus",
                  "gnetId": 7636,
                  "revision": 1,
                },
                "istio_workload": {
                  "datasource": "Prometheus",
                  "gnetId": 7630,
                  "revision": 1,
                },
              },
            },
          },
          "coreDns": {
            "enabled": 'false',
          },
          "kubeDns": {
            "enabled": 'true',
          },
          "kubeControllerManager": {
            "enabled": 'false',
          },
          "kubeScheduler": {
            "enabled": 'false',
          },
        },
       repository_opts=RepositoryOptsArgs(
           repo="https://prometheus-community.github.io/helm-charts",
       ),
   ), #opts=ResourceOptions(provider=k8s_provider)
)

# prometheus_stack = Chart(
#    "prometheus-stack",
#    ChartOpts(
#        chart="kube-prometheus-stack",
#        namespace=PROMETHEUS_NAMESPACE,
#         version="25.0.0",
#         values={
#             'prometheus':{
#                    'prometheusSpec':{
#                        'retention': '26w',
#                        'scrapeInterval': '15s',
#                        'storageSpec':{
#                            'volumeClaimTemplate':{
#                                'spec':{
#                                    'accessModes': '["ReadWriteOnce"]',
#                                    'storageClassName': 'standard',
#                                    'resources':{
#                                        'requests':{
#                                            'storage': '30Gi',
#                                            },
#                                        },
#                                    },
#                                },
#                            },
#                        },
#                },
#             'alertmanager':{
#                'config':{
#                    'alertmanagerSpec':{
#                        'configSecret': 'alertmanager-prometheus-stack-custom-prometheus-op-alertmanager',
#                        },
#                    },
#                },
#             'grafana':{
#                'adminPassword': 'YzljYzZmZWY5Njc0NzJhNDVhYWZjYjU2',
#                'persistence':{
#                    'enabled': 'true',
#                    'type': 'statefulset',
#                    'size': '1Gi',
#                    },
#                "dashboardProviders": {
#                     "dashboardproviders.yaml": {
#                       "apiVersion": 1,
#                       "providers": [
#                         {
#                           "allowUiUpdates": true,
#                           "disableDeletion": true,
#                           "editable": true,
#                           "folder": "Istio",
#                           "folderUid": "",
#                           "name": "istio",
#                           "options": {
#                             "path": "/var/lib/grafana/dashboards/istio"
#                           },
#                           "orgId": 1,
#                           "type": "file",
#                           "updateIntervalSeconds": 10
#                         },
#                         {
#                           "allowUiUpdates": true,
#                           "disableDeletion": true,
#                           "editable": true,
#                           "folder": "Community",
#                           "folderUid": "",
#                           "name": "community",
#                           "options": {
#                             "path": "/var/lib/grafana/dashboards/community"
#                           },
#                           "orgId": 1,
#                           "type": "file",
#                           "updateIntervalSeconds": 10
#                         },
#                         {
#                           "allowUiUpdates": true,
#                           "disableDeletion": true,
#                           "editable": true,
#                           "folder": "Custom",
#                           "folderUid": "",
#                           "name": "custom",
#                           "options": {
#                             "path": "/tmp/dashboards/custom"
#                           },
#                           "orgId": 1,
#                           "type": "file",
#                           "updateIntervalSeconds": 10
#                         }
#                       ]
#                     }
#                   },
#                 "dashboards": {
#                     "community": {
#                       "elastisearch": {
#                         "datasource": "Prometheus",
#                         "gnetId": 2322,
#                         "revision": 4
#                       }
#                     },
#                     "istio": {
#                       "istio_galley": {
#                         "datasource": "Prometheus",
#                         "gnetId": 7648,
#                         "revision": 1
#                       },
#                       "istio_mesh": {
#                         "datasource": "Prometheus",
#                         "gnetId": 7639,
#                         "revision": 1
#                       },
#                       "istio_mixer": {
#                         "datasource": "Prometheus",
#                         "gnetId": 7642,
#                         "revision": 1
#                       },
#                       "istio_pilot": {
#                         "datasource": "Prometheus",
#                         "gnetId": 7645,
#                         "revision": 1
#                       },
#                       "istio_service": {
#                         "datasource": "Prometheus",
#                         "gnetId": 7636,
#                         "revision": 1
#                       },
#                       "istio_workload": {
#                         "datasource": "Prometheus",
#                         "gnetId": 7630,
#                         "revision": 1
#                       }
#                     }
#                 },
#             'coreDns':{
#                'enabled': 'false',
#             },
#             'kubeDns':{
#                'enabled': 'true',
#             },
#             'kubeControllerManager':{
#                'enabled': 'false',
#             },
#             'kubeScheduler':{
#                'enabled': 'false',
#             },
#         },
#        fetch_opts=FetchOpts(
#            repo="https://prometheus-community.github.io/helm-charts",
#        ),
#    ), #opts=ResourceOptions(provider=k8s_provider)
# )
## Prometheus rules
prometheus_rules = ConfigGroup(
  "prometheus-rules",
  files=["prometheus-stack/prometheus-rules/*.yaml"])

## Prometheus Service monitors
prometheus_servicemonitors = ConfigGroup(
  "prometheus-servicemonitors",
  files=["prometheus-stack/service-monitors/*.yaml"])


alerts = open("./grafana/dashboards/alerts-firing-dashboard.json", "r")

 ## Grafana dashboards
grafana_alerts = ConfigMap(
    "grafana-alerts",
    metadata={
        "name" : "alerts-firing-dashboard",
        "namespace": "monitoring",
        "labels" : {
            "release": "prometheus-stack",
            "app": "prometheus-stack-grafana",
            "grafana_dashboard": "1",
        },
        "annotations": { "k8s-sidecar-target-directory" : "/tmp/dashboards/custom" },
    },
    data={
        "alerts-firing-dashboard.json": alerts.read(),
    },
)
alerts.close()

# grafana_alerts_annotate = local.Command("annotate",
#     create="kubectl -n monitoring annotate --overwrite=true configmap alerts-firing-dashboard k8s-sidecar-target-directory='/tmp/dashboards/custom'"
# )

drive = open("./grafana/dashboards/drive-stats-dashboard.json", "r")
grafana_drive = ConfigMap(
    "grafana-drive",
    metadata={
        "name" : "drive-stats-dashboard",
        "namespace" : "monitoring",
        "labels" : {
            "release": "prometheus-stack",
            "app": "prometheus-stack-grafana",
            "grafana_dashboard": "1",
        },
        "annotations": { "k8s-sidecar-target-directory" : "/tmp/dashboards/custom" },
    },
    data={
        "drive-stats-dashboard.json": drive.read(),
    },
)
drive.close()

go_processes = open("./grafana/dashboards/go-processes-dashboard.json", "r")
grafana_go = ConfigMap(
    "grafana-go",
    metadata={
        "name" : "go-processes-dashboard",
        "namespace": "monitoring",
        "labels" : {
            "release": "prometheus-stack",
            "app": "prometheus-stack-grafana",
            "grafana_dashboard": "1",
        },
        "annotations": { "k8s-sidecar-target-directory" : "/tmp/dashboards/custom" },
    },
    data={
        "go-processes-dashboard.json": go_processes.read(),
    },
)
go_processes.close()

tracing = open("./grafana/dashboards/tracing-infrastructure-dashboard.json", "r")
tracing_infrastructure = ConfigMap(
    "tracing-infrastructure",
    metadata={
        "name" : "tracing-infrastructure-dashboard",
        "namespace": "monitoring",
        "labels" : {
            "release": "prometheus-stack",
            "app": "prometheus-stack-grafana",
            "grafana_dashboard": "1",
        },
        "annotations": { "k8s-sidecar-target-directory" : "/tmp/dashboards/custom" },
    },
    data={
        "tracing-infrastructure-dashboard.json": tracing.read(),
    },
)
tracing.close()

### Namespace Init ###

if CREATE_NAMESPACE:

    ### Create DNS Records ###
    dns_record_set = gcp.dns.RecordSet("Record-Set",
    name=f"{SERVICES_NAMESPACE}.{DNS_BASE_NAME}.",
    managed_zone=dns_zone.name,
    type="A",
    ttl=300,
    rrdatas=[srv.status.load_balancer.ingress[0].ip])

    dns_record_set_wildcard = gcp.dns.RecordSet("Record-Set-Wildcard",
    name=f"*.{SERVICES_NAMESPACE}.{DNS_BASE_NAME}.",
    managed_zone=dns_zone.name,
    type="A",
    ttl=300,
    rrdatas=[srv.status.load_balancer.ingress[0].ip])

    services_namespace = Namespace('services-namespace',
        metadata={
            "name" : SERVICES_NAMESPACE,
            "labels" : appLabels,
    })

    HOSTNAME = f'{SERVICES_NAMESPACE}.{DNS_BASE_NAME}'

    # def prepare_gateway(obj):
    #     if obj['kind'] == "Gateway" and obj['metadata']['name'] == f"gateway-{SERVICES_NAMESPACE}":
    #         obj['spec']['selector']['istio'] = "ingressgateway",
    #         obj['spec']['server']['ports']['hosts'] = [f'*.{HOSTNAME}',f'{HOSTNAME}']

    create_gateway_template = local.Command("create-gateway-template",
        create=f"renderizer -C --namespace={SERVICES_NAMESPACE} --hostname='{SERVICES_NAMESPACE}.{DNS_BASE_NAME}' --ingressGateway='ingressgateway' ./yaml/gateway-template.yaml > ./gen-files//gateway-{SERVICES_NAMESPACE}.yaml"
    )

    create_certificate_template = local.Command("create-certificate-template",
        create=f"renderizer -C --namespace={SERVICES_NAMESPACE} --hostname='{SERVICES_NAMESPACE}.{DNS_BASE_NAME}' --clusterIssuer='letsencrypt-dns' ./yaml/certificate-lets-encrypt-template.yaml > ./gen-files/certificate-{SERVICES_NAMESPACE}.yaml"
    )

    gateway = kubernetes.yaml.ConfigFile('gateway', f'./gen-files/gateway-{SERVICES_NAMESPACE}.yaml', #opts=ResourceOptions(provider=k8s_provider,depends_on=[istio_base,istio_ingress]))
    certificate = kubernetes.yaml.ConfigFile('certificate', f'./gen-files/certificate-{SERVICES_NAMESPACE}.yaml', #opts=ResourceOptions(provider=k8s_provider,depends_on=[gateway,istio_base,istio_ingress]))

    ### To create gitlab secret first I run
    # $ kubectl create secret docker-registry regsec_dry --docker-server=registry.gitlab.com --docker-username=user --docker-email=email --docker-password=gitlab_token --dry-run -o yaml | grep .dockerconfigjson: | sed -e 's/.dockerconfigjson://' | sed -e 's/^[ \t]*//'
    # The previous command will display a Base64 string which I save with:
    # $ pulumi config set gcp-python-serviceacount:DOCKER_TOKEN <output_from_previous_command> --secret
    # This will create a secret entry on the Pulumiv.dev.yaml containing the token to create the dockerconfigjson secret to pull images from gitlab
    # https://overflowed.dev/blog/how-to-create-a-kubernetes-docker-registry-secret-with-pulumi/#the-pulumi-secret

    gitlab_secret = Secret("gitlab_secret",
            type='kubernetes.io/dockerconfigjson',
            data={
                ".dockerconfigjson": DOCKER_TOKEN,
            },
            metadata={
                "name" : 'gitlab-registry',
                "namespace": f'{SERVICES_NAMESPACE}',
            },
    )

    if INIT_NAMESPACE:

        minio_account = gcp.serviceaccount.Account("minio-account",
            account_id=f"minio-{SERVICES_NAMESPACE}",
            description="",
            disabled=False,
            display_name=f"minio-{SERVICES_NAMESPACE}",
            project=proj,
            )


        minio_member=[Output.concat("serviceAccount:", minio_account.email)]

        minio_account_iam = gcp.projects.IAMBinding("minio-account-iam",
            project=proj,
            role="roles/storage.admin",
            members=minio_member)

        create_jwt_secret = local.Command("create-jwt-secret",
            create=f"bash scripts/create-auth-jwt-secret.sh --name={AUTH_SECRET_NAME} --output-file='gen-files/{AUTH_JWT_SECRET}' --namespace={SERVICES_NAMESPACE}"
        )

        auth_jwt_secret = kubernetes.yaml.ConfigFile('auth-jwt-secret', f'./gen-files/{AUTH_JWT_SECRET}')

        minio_key = gcp.serviceaccount.Key("minio-Key",
            service_account_id=minio_account.name,
            public_key_type="TYPE_X509_PEM_FILE",
            )

        MINIO_SECRET_KEY_PRE = hashlib.sha256(str(int(1656372677)).encode('utf-8')).hexdigest()
        MINIO_SECRET_KEY = str(base64.b64encode(MINIO_SECRET_KEY_PRE.encode('utf-8')), "utf-8")

        minio_secret = Secret("minio-gcs",
                type='Opaque',
                data={
                    "accesskey": str(base64.b64encode(MINIO_ACCESS_KEY.encode("utf-8")), "utf-8"),
                    "secretkey": MINIO_SECRET_KEY,
                    "gcs_key.json": minio_key.private_key,
                },
                metadata={
                    "name" : f'{MINIO_GCS_SECRET_NAME}',
                    "namespace": f'{SERVICES_NAMESPACE}',
                },
        )

        SORS_USERS_LIST = []
        for i in range(len(SORS_USERS)):
            SORS_USERS_LIST.append(str(SORS_USERS[i]) + ':' + str(hashlib.sha256(str(int(1656372677)).encode('utf-8')).hexdigest().encode('utf-8'), "utf-8"))

        sors_users_secret = Secret("sors-users",
                type='Opaque',
                data={
                    "users": str(base64.b64encode(str(SORS_USERS_LIST).encode("utf-8")), "utf-8"),
                },
                metadata={
                    "name" : f'{SORS_USERS_SECRET_NAME}',
                    "namespace": f'{SERVICES_NAMESPACE}',
                },
        )

        report_sa_key = gcp.serviceaccount.Key("report-sa-key",
            service_account_id=report_account.name,
            public_key_type="TYPE_X509_PEM_FILE",
            )

        report_account_secret = Secret("report-serviceaccount-credentials",
                type='Opaque',
                data={
                    "service_account_credentials.json": report_sa_key.private_key,
                },
                metadata={
                    "name" : f'{REPORT_SA_SECRET}',
                    "namespace": f'{SERVICES_NAMESPACE}',
                },
        )


        project_sa_key = gcp.serviceaccount.Key("project-sa-key",
            service_account_id=project_account.name,
            public_key_type="TYPE_X509_PEM_FILE",
            )

        project_account_secret = Secret("project-serviceaccount-credentials",
                type='Opaque',
                data={
                    "service_account_credentials.json": project_sa_key.private_key,
                },
                metadata={
                    "name" : f'{PROJECT_SA_SECRET}',
                    "namespace": f'{SERVICES_NAMESPACE}',
                },
        )

        otdr_sa_key = gcp.serviceaccount.Key("otdr-sa-key",
            service_account_id=otdr_account.name,
            public_key_type="TYPE_X509_PEM_FILE",
            )

        otdr_account_secret = Secret("sqlproxy-secret",
                type='Opaque',
                data={
                    "service_account_credentials.json": otdr_sa_key.private_key,
                },
                metadata={
                    "name" : f'{OTDR_SA_SECRET}',
                    "namespace": f'{SERVICES_NAMESPACE}',
                },
        )

        # Create a GCP resource (Storage Bucket)
        minio_bucket = storage.Bucket(MINIO_BUCKET_NAME,name=MINIO_BUCKET_NAME)

        ### Create Databases

        report_database = gcp.sql.Database("report-database",
            instance=postgres_instance.name,
            name=f'reportservice_{SERVICES_NAMESPACE}',
        )

        project_database = gcp.sql.Database("project-database",
            instance=postgres_instance.name,
            name=f'projectservice_{SERVICES_NAMESPACE}',
        )

        ### Create dummy aeros-link-core configMap
        aeros_link_core = kubernetes.yaml.ConfigFile('aeros-link-core', './yaml/aeros-link-core.yaml', #opts=ResourceOptions(provider=k8s_provider,depends_on=[services_namespace]))

        AUTH_SERVICE_VERSION = config.get('AUTH_SERVICE_VERSION')
        AUTH_SERVICE_AEROS_HOST = config.get('AUTH_SERVICE_AEROS_HOST')

        create_auth_template = local.Command("create-auth-template",
            create=f"renderizer -C --Namespace={SERVICES_NAMESPACE} --Namespaceshort={SERVICES_NAMESPACE} --Domainname={DOMAIN_NAME} ./yaml/auth-values-gke-template.yaml > ./gen-files/auth-values-gke-{SERVICES_NAMESPACE}.yaml"
        )

        auth_service = Release(
            "auth-service",
            ReleaseArgs(
                name=f"{SERVICES_NAMESPACE}-auth",
                chart="./helm/auth-service",
                value_yaml_files=[pulumi.FileAsset(f"./gen-files/auth-values-gke-{SERVICES_NAMESPACE}.yaml")],
                namespace=SERVICES_NAMESPACE,
                values={
                    "analytics": {
                        "id": "UA-154682061-1",
                    },
                    "image": {
                        "repository": "registry.gitlab.com/afl-global/aeros-auth-service/auth-service",
                        "tag": AUTH_SERVICE_VERSION,
                    },
                    "aeros": {
                        "host": AUTH_SERVICE_AEROS_HOST,
                    },
                },
            ),
        )

        rabbitmq_service = Release(
            "rabbitmq-service",
            ReleaseArgs(
                name=f"{SERVICES_NAMESPACE}-rabbitmq",
                chart="rabbitmq",
                version="10.1.8",
                repository_opts=RepositoryOptsArgs(
                    repo="https://charts.bitnami.com/bitnami",
                ),
                #value_yaml_files=[pulumi.FileAsset(f"./yaml/rabbitmq-gke-values.yaml")],
                values={
                    "auth": {
                        "password": RABBITMQ_PASSWORD,
                    },
                },
                namespace=SERVICES_NAMESPACE,
            ),
        )

        # SORS_SERVICE_VERSION = config.get('SORS_SERVICE_VERSION')
        # SORS_SERVICE_AEROS_HOST = config.get('SORS_SERVICE_AEROS_HOST')
        #
        # create_sors_template = local.Command("create-sors-template",
        #     create=f"renderizer -C --Namespace={SERVICES_NAMESPACE} --Namespaceshort={SERVICES_NAMESPACE} --Domainname={DOMAIN_NAME} ./yaml/sors-values-gke-template.yaml > ./gen-files/sors-values-gke-{SERVICES_NAMESPACE}.yaml"
        # )
        #
        # sors_service = Release(
        #     "sors-service",
        #     ReleaseArgs(
        #         name=f"{SERVICES_NAMESPACE}-sors",
        #         dependency_update=True,
        #         chart="./helm/sors",
        #         value_yaml_files=[pulumi.FileAsset(f"./gen-files/sors-values-gke-{SERVICES_NAMESPACE}.yaml")],
        #         namespace=SERVICES_NAMESPACE,
        #         values={
        #             "analytics": {
        #                 "id": "UA-154682061-1",
        #             },
        #             "broker": {
        #                 "user": "user",
        #                 "password": "x3P8xwnz4s",
        #             },
        #             "image": {
        #                 "repository": "registry.gitlab.com/afl-global/aeros-auth-service/auth-service",
        #                 "tag": AUTH_SERVICE_VERSION,
        #             },
        #             "aeros": {
        #                 "host": AUTH_SERVICE_AEROS_HOST,
        #             },
        #         },
        #     ),
        # )

# Finally, export the kubeconfig so that the client can easily access the cluster.
export('kubeconfig', k8s_config)

# Export the service account Name
pulumi.export('service account', dns_service_account.account_id )

# Export the DNS name of the bucket
pulumi.export('minio_bucket', minio_bucket.url)

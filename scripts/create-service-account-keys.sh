abort() {
    echo "⛔️ Error encountered... aborting initialization."
    exit 1
}

error() {
    if [ $# -lt 1 ]; then abort; fi

    echo "❌ $1"

    if [ $# -gt 1 ]
    then
        echo "----------"
        cat $2
        echo "----------"
    fi

    abort;
}

warning() {
    if [ $# -lt 1 ]; then abort; fi

    echo "⚠️  $1"

    if [ $# -gt 1 ]
    then
        echo "----------"
        cat $2
        echo "----------"
    fi
}

status() {
    if [ $# -lt 1 ]; then return; fi

    echo "   $1"
}

success() {
    if [ $# -lt 1 ]; then return; fi

    echo "✅ $1"
}


create_service_account_keys() {
    if [ $# -lt 3 ]
    then
        echo "Error: add_service_account_role(): wrong number of arguments!"
        error "usage: add_service_account_role GCP_project service_account_email key-file"
    fi

    PRJ="$1"
    SA="$2"
    KEY="$3"

    RESPONSE=$(gcloud --project=$PRJ iam service-accounts keys create $KEY --iam-account $SA 2> error.log || echo "KeyCreateError")
    if [[ $RESPONSE = "KeyCreateError" ]]
    then
        error "Error creating key for service account." error.log
    fi
}

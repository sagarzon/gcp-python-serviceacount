#!/bin/sh

GEN_SECRET_FILE=true
SHARED_FILE="auth-jwt-secret.json"
SECRET_NAME="aeros-auth-jwt-secret"
SECRET_FILE="auth-jwt-secret.yaml"


for i in "$@"
do
case $i in
    -s=*|--secret-file=*)
    SHARED_FILE="${i#*=}"
    shift # past argument=value
    ;;
    -o=*|--output-file=*)
    SECRET_FILE="${i#*=}"
    shift # past argument=value
    ;;
    -n=*|--name=*)
    SECRET_NAME="${i#*=}"
    shift # past argument=value
    ;;
    --namespace=*)
    NAMESPACE="${i#*=}"
    shift
    ;;
    #-u|--update)
    #GIT_UPDATE_FIRST=true
    #shift # past argument with no value
    #;;
    --help)
    SHOW_HELP=true
    shift # past argument with no value
    ;;
    *)
            # unknown option
    ;;
esac
done

if [[ -n $SHOW_HELP ]]; then
    echo "usage: $0 [--ip=addr] [--name secret_name] [--output-file=ouput_filename] [--help]"
    echo "    --ip           - specify the IP address of the hostname [default: $HOST_IP]"
    echo "    --name         - specify ingress name [default: $SECRET_NAME]"
    echo "    --output-file  - specify filename of the filename [default: $SECRET_FILE]"
    echo "    --help         - display this message"

    exit 0
fi

echo "GEN_SECRET_FILE   : $GEN_SECRET_FILE"
echo "SHARED_FILE       : $SHARED_FILE"
echo "SECRET_NAME       : $SECRET_NAME"
echo "SECRET_FILE       : $SECRET_FILE"

OUTDIR=$(dirname $SECRET_FILE)

if [[ -n $GEN_SECRET_FILE ]]; then
    echo "Generating new secret..."
    THE_SECRET="$(uuidgen)"
    # place the secret in same directory as output file
    SHARED_FILE_PATH="$OUTDIR/$SHARED_FILE"

    echo "Creating secret file..."
    cat << EOS >$SHARED_FILE_PATH
{
  "secret": "$THE_SECRET"
}
EOS
else
    # user provided shared file, use path provided
    SHARED_FILE_PATH=$SHARED_FILE
fi


# Backup existing yaml file
if [[ -f "$SECRET_FILE" ]]; then
    mv -f $SECRET_FILE $SECRET_FILE.bak
fi


# Create the Kubernetes Secret YAML File
#
cat <<EOM >$SECRET_FILE
# aeRos Auth JWT Secret
apiVersion: v1
kind: Secret
metadata:
  namespace: $NAMESPACE
  name: $SECRET_NAME
type: Opaque
data:
  $SHARED_FILE: $(cat $SHARED_FILE_PATH | base64)
EOM

echo Done!

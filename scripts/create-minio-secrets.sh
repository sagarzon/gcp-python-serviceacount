#!/bin/sh

backupFile () {
  if [[ -f "$1" ]]; then
    echo "Backing up $1 ..."
    mv -f $1 $1.bak
  fi
}


MINIO_ACCESS_KEY="sors-minio"
MINIO_SECRET_KEY="password"   # default: will result in auto-gen random password
GCS_KEY_FILE="gcs_key.json"
MINIO_GCS_SECRET_NAME="minio-gcs"
MINIO_GCS_SECRET_FILE="minio-gcs-secret.yaml"


for i in "$@"
do
case $i in
    -g=*|--gcs-key-file=*)
    GCS_KEY_FILE="${i#*=}"
    shift # past argument=value
    ;;
    -k=*|--access-key=*)
    MINIO_ACCESS_KEY="${i#*=}"
    shift # past argument=value
    ;;
    -n=*|--name=*)
    MINIO_GCS_SECRET_NAME="${i#*=}"
    shift # past argument=value
    ;;
    -o=*|--output-file=*)
    MINIO_GCS_SECRET_FILE="${i#*=}"
    shift # past argument=value
    ;;
    -s=*|--secret-key=*)
    MINIO_SECRET_KEY="${i#*=}"
    shift # past argument=value
    ;;
    --namespace=*)
    NAMESPACE="${i#*=}"
    shift # past argument=value
    ;;
    #-u|--update)
    #GIT_UPDATE_FIRST=true
    #shift # past argument with no value
    #;;
    --help)
    SHOW_HELP=true
    shift # past argument with no value
    ;;
    *)
            # unknown option
    ;;
esac
done

if [[ -n $SHOW_HELP ]]; then
    echo "usage: $0 [--gcs-key-file=key_file] [--access-key=name] [--secret-key=secret] [--name=secret-name] [--help]"
    echo "    --gcs-key-file  - specify GCS credentials file [default: $GCS_KEY_FILE]"
    echo "    --access-key    - specify Minio Access Key [default: $MINIO_ACCESS_KEY]"
    echo "    --secret-key    - specify Minio Secret Key [default: $MINIO_SECRET_KEY (auto-gen new random secret)]"
    echo "    --name          - specify secret name [default: $MINIO_GCS_SECRET_NAME]"
    echo "    --help          - display this message"

    exit 0
fi


# Generate MINIO Secret (if none was provided on command-line)
if [ "$MINIO_SECRET_KEY" = "password" ]; then
  # no password supplied, generate a random one
  MINIO_SECRET_KEY=`date +%s | sha256sum | base64 | head -c 32`
fi

echo "MINIO_ACCESS_KEY           : $MINIO_ACCESS_KEY"
echo "MINIO_SECRET_KEY           : $MINIO_SECRET_KEY"
echo "GCS_KEY_FILE               : $GCS_KEY_FILE"
echo "MINIO_GCS_SECRET_NAME      : $MINIO_GCS_SECRET_NAME"
echo "MINIO_GCS_SECRET_FILE      : $MINIO_GCS_SECRET_FILE"


# Backup existing yaml files
backupFile $MINIO_GCS_SECRET_FILE


# Create the Minio-GCS Secret YAML File
#
echo "Creating Minio-GCS Secret YAML file..."
cat <<EOM >$MINIO_GCS_SECRET_FILE
# aeRos Auth JWT Secret
apiVersion: v1
kind: Secret
metadata:
  name: $MINIO_GCS_SECRET_NAME
  namespace: $NAMESPACE
type: Opaque
data:
  accesskey: $(echo "$MINIO_ACCESS_KEY" | tr -d '\n' | base64)
  secretkey:  $(echo $MINIO_SECRET_KEY | tr -d '\n' | base64)
  gcs_key.json: $(cat $GCS_KEY_FILE | base64 --wrap=0)
EOM

echo Done!
